/* item-struct-union.m : class for the 'struct-union'  grammar element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"


/////// STRUCT SPECIFIERS ////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% /
@implementation Weasel_Struc_Union_Specifier

-initWith: (char *)st_or_un
identifier: (id) idt
declaration_list: (id)list
{
  [super init];
  self->struct_or_union = st_or_un;
  self->identifier = idt;
  self->declaration_list = list;

  return self;
}

- sgmlToFile : (FILE *)fd
{

  fprintf(fd, "<PARA>\n");
  if (identifier)
    [identifier sgmlToFile:fd];
  if (struct_or_union)
    fprintf(fd," (%s)",struct_or_union );
  fprintf(fd,"\n{");
  if (declaration_list)
    [declaration_list sgmlToFile:fd];
  
  fprintf(fd,"\n}");
  fprintf(fd, "</PARA>\n");
  return self;
}


@end


@implementation Weasel_Struct_Declaration

- initWithTypeList: (id)tlist
    declaratorList: (id)dlist
{
  [super init];
  self->type_specifier_list = tlist;
  self->declarator_list = dlist;
  
  return self;
}


- sgmlToFile : (FILE *)fd
{

  fprintf(fd, "\n");
  if (type_specifier_list)
    [type_specifier_list sgmlToFile:fd];
  if (declarator_list)
    [declarator_list sgmlToFile:fd];
   fprintf(fd, "\n");
 
  return self;
}

@end

@implementation Weasel_Struct_Declaration_List
@end


@implementation Weasel_Struct_Declarator

- initWithDeclarator: (id)decl
	 initializer: (id)init
{
  [super init];
  self->declarator = decl;
  self->initializer = init;
  
  return self;
}


- sgmlToFile : (FILE *)fd
{

  if (declarator)
    [declarator sgmlToFile:fd];
  if (initializer)
    {
      fprintf(fd,"(=");
      [initializer sgmlToFile:fd];
      fprintf(fd,")");
    }
 
  return self;
}


@end


@implementation Weasel_Struct_Declarator_List 
@end


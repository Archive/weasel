/* item-struct-union.h : class for the 'struct-union'  grammar element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ITEM_STRUCT_UNION_H
#define ITEM_STRUCT_UNION_H

#include "item.h"

/////// STRUCT SPECIFIERS ////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% /
@interface Weasel_Struc_Union_Specifier: Weasel_Item
{
  char *struct_or_union; // contains 'struct' or 'union'
  id identifier; // contains the name of the struct type
  id declaration_list;
}

-initWith: (char *)st_or_un
identifier: (id) idt
declaration_list: (id)list;

@end


@interface Weasel_Struct_Declaration: Weasel_Item
{
  id type_specifier_list;
  id declarator_list;
}

- initWithTypeList: (id)tlist
    declaratorList: (id)dlist;

@end

@interface Weasel_Struct_Declaration_List: Weasel_Item_List
@end


@interface Weasel_Struct_Declarator: Weasel_Item
{
  id declarator;
  id initializer;
}

- initWithDeclarator: (id)decl
	 initializer: (id)init;

@end


@interface Weasel_Struct_Declarator_List: Weasel_Item_List
@end





#endif /* ITEM_STRUCT_UNION_H */

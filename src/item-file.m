/* item-file.m : class defining a 'file' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"
////////////////                 ////////////////
@implementation Weasel_File

/** 
 * add an external declarartion to the list the cuirrent 
 * file 
 */
- addExternDeclaration : (id)dl
{
   
  self->declaration_list = g_list_append( self->declaration_list, (gpointer)dl );
  return self;
}

- sgmlToFile : (FILE *)fd
{
  GList *current;
  int nbElem, i;
 
  current = self->declaration_list;
  nbElem = g_list_length(current);
  fprintf(fd,"\n<sect1><title>Functions</title>\n");

  for (i=0; i<nbElem; i++)
     {
       /* list the function definitions */
       if ([(id)(current->data) isFuncDef]) 
	 {
	   [(id)(current->data) sgmlToFile :fd];
	 }
       current=g_list_next(current);
     }
  fprintf(fd,"\n</sect1>\n");

#ifdef NOTDEF
  current = self->declaration_list;
  fprintf(fd,"\n<sect1><title>Other Declarations</title>");

   for (i=0; i<nbElem; i++)
     {
       /* list the other declarations */
       if (![(id)(current->data) isFuncDef]) 
	 {
	   [(id)(current->data) sgmlToFile :fd];
	 }
       current=g_list_next(current);
     }
  fprintf(fd,"\n</sect1>");
#endif /* NOTDEF */
  return self;
  
}


/** 
 * add its own declaration to the list f
 */
- addToFelmList:(id)f
{
  GList *current;
  int nbElem, i;
 
  current = self->declaration_list;
  nbElem = g_list_length(current);

  for (i=0; i<nbElem; i++)
     {
       /* list the function definitions */
       if ([(id)(current->data) isFuncDef]) 
	 {
	   [(id)(current->data) addToFelmList:f];
	 }
       current=g_list_next(current);
     }


}

@end




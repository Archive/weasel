/* item-declarator.h : class defining a 'declarator' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ITEM_DECLARATOR_H
#define ITEM_DECLARATOR_H 1

#include "item.h"


///////   DECLARATORS     ////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% /
@interface Weasel_Declarator : Weasel_Item
{ 
  id declarator;
  int withBracket;
  id pointer;

}
- initWithDeclarator: (id) d;
- setDeclarator: (id)d; 
- addBracket;
- addPointer : (id)p;
@end

//////////////////////////////////////////////////
@interface Weasel_Function_Declarator : Weasel_Declarator
{ 
  id param_list;
}

- initWithDeclarator: (id) d
	   paramList: (id ) plist;

- setParamList: (id )plist;
- sgmlFunctionNameToFile : (FILE *)fd;
- sgmlParamListToFile : (FILE *)fd;
- sgmlParamDescriptionToFile : (FILE *)fd;

@end

@interface Weasel_Array_Declarator : Weasel_Declarator
{ 
  id index;
}
- initWithDeclarator: (id) d
	  index: (id ) ind;
- setIndex: (id )ind;
@end



#endif /* ITEM_DECLARATOR_H */

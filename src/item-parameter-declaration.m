/* item-parameter-declaration.h : class defining a 'parameter-declaration' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* item-parameter-declaration.m : class defining a 'parameter-declaration' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"
#include "api-comment.h"

//////////////// PARAMETERS ///////////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% //
@implementation Weasel_Parameter_Declaration

- initWithTypeSpec: (id)ts
	 decl: (id)decl
{
  [super init];
  self->type_spec_lst=ts;
  self->declarator=decl;
  return self;
}

- initWithTypeName: (id)tn
{
  [super init];
  self->type_name=tn;
  return self;
}

- sgmlToFile : (FILE *)fd
{
  fprintf(fd,"<paramdef>");
  if (self->type_spec_lst)
    [type_spec_lst sgmlToFile :fd];
  if (self->declarator)
    {
      fprintf(fd,"<parameter>");
      [declarator sgmlToFile :fd];
      fprintf(fd,"</parameter>");
    }
  fprintf(fd,"</paramdef>\n");
  return self;
}

- sgmlDescriptionToFile : (FILE *)fd
{
  char *paramName;
  paramName = [declarator getName];
fprintf(fd,"<varlistentry><term><parameter>%s</parameter>\
</term>\n<listitem><para>%s</para></listitem>\
</varlistentry>\n", paramName, comment_line);
 free(paramName);

 return self;
}



- addComment: (id)c
{
  char *paramName;

  paramName = [declarator getName];
  comment_line = [c getValueFromKey:paramName];
  free( paramName );
  return self;
}
@end

@implementation Weasel_Parameter_Declaration_List

- addElipsisParameter
{
  self->with_ellipsis=1;
  return self;
}

- sgmlToFile : (FILE *)fd
{
  [super sgmlToFile :fd];
  if (self->with_ellipsis)
    fprintf(fd,"<paramdef><parameter>...</parameter></paramdef>\n");

  return self;
}

- sgmlDescriptionToFile : (FILE *)fd
{
  guint nbElem;
  GList *current;
  int i;
 
  nbElem = g_list_length(children);
  current = children;
  fprintf(fd,"<variablelist>\n");
  for (i=0; i<nbElem; i++)
    {	   
      [(id)current->data sgmlDescriptionToFile:fd];
      current = g_list_next( current );
    }
  fprintf(fd,"</variablelist>\n");
  return self;
}

@end

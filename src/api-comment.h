/* api-comment.h : classes defining an documentation comment
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef API_COMMENT_H
#define API_COMMENT_H 1



#include <string.h>
#include <objc/Object.h>
#include "glib.h"

/////////////////////////////////////
@interface Weasel_Api_Line : Object
/////////////////////////////////////
{
  char *line;
}
- initWithString: (char *) l;
- (char *)getLine;
@end

/////////////////////////////////////
@interface Weasel_Api_Line_List : Object
/////////////////////////////////////
{
  GList *line_list;
}
- initWithLine: (id) l;
- addLine: (id)l;
- (char *)getLine;
@end


/////////////////////////////////////
@interface Weasel_Api_Field : Object
/////////////////////////////////////
{
  char *key;
  id value; // a line 
}

- initWithKey: (char *) k
       value: (id)v;
- (char *)getKey;
- (id)getValue;
- (char)matchKey: (char *)k;

@end

/////////////////////////////////////
@interface Weasel_Api_Param : Weasel_Api_Field
/////////////////////////////////////
@end


/////////////////////////////////////
@interface  Weasel_Api_Field_List : Object
/////////////////////////////////////
{
  GList *fields;
}
- initWithField:(id)f;
- addField: (id)f;
- (GList *)getList;
- (char *)getValueFromKey: (char *)k;

@end


/////////////////////////////////////
@interface Weasel_Api_Comment : Object
/////////////////////////////////////
{
  // params are fields which key was preceeded by a "@"
  // they are called param because of their meaning in 
  // the case of a function comment
  // but for struct/enum/union, they are field descriptors.
  id fieldList;
}
- initWithFieldList : (id)fl;
- (char *)getValueFromKey: (char *)k;

@end

#endif /* API_COMMENT_H */

/* item-root.h : class containing all the declarations found
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#ifndef INCLUDE_ITEM_ROOT_H
#define INCLUDE_ITEM_ROOT_H 1

///////////////////////////////////////////
/////////// Root //////////////////////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% //
@interface Weasel_Root : Weasel_Item
{
   GList *type_list;
   GList *file_list;
   @public
   id fileElements;
}
- addNewTypedef: (char *)nt;
- (int)knowsType: (char *)t;
- getFile : (char *)filename;
- storeFile : (char *)filename
declaration: (id)dl;

- sgmlToFile : (FILE *)fd;
- createFelmList;
@end

// flags in the #line cpp output 
@interface Weasel_Flag_List : Weasel_Item_List 
- (int)isSomeNewText;
@end



#endif /* INCLUDE_ITEM_ROOT_H */

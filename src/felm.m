/* felm.m : parent class for all the file elements classes
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "felm.h"

////////////////////////////////
@implementation felm_string
////////////////////////////////
- initWithString: (char *)s
{
  [super init];
  str = strdup(s);
  return self;
}

- (char *)getString
{
  if (str)
    return strdup(str);
  else
    return strdup("");
}


- sgmlToFile : (FILE *)fd
{
  fprintf(fd, "%s", str);
  return self;
}

@end


////////////////////////////////
@implementation felm
////////////////////////////////
- sgmlToFile : (FILE *)fd
{
  
  fprintf(fd, "\nName : ");
  [name sgmlToFile:fd] ;
  
  fprintf(fd, "\nDescription : ");
  [desc sgmlToFile:fd];
  
  fprintf(fd, "\nSee also : ");
  [see_also sgmlToFile:fd];
  
  return self;
}

- setName: (id)n
{
  name = n;
  return self;
}

- setDesc: (id)d
{
  desc = d;
  return self;
}


- setSeeAlso : (id)sa
{
  see_also = sa;
  return self;
}

@end

////////////////////////////////
@implementation felm_function
////////////////////////////////



- setParamList: (id)pl
{
  param_list = pl;
  return self;
}


- sgmlToFile : (FILE *)fd
{
  fprintf(fd,"<FUNCSYNOPSIS>\n");
  // write the return type
  [return_type sgmlToFile:fd];

  fprintf(fd,"\n<FUNCDEF>\n"); 
  [name sgmlToFile:fd];
  fprintf(fd,"\n</FUNCDEF>\n");

  // list the parameters here.

  fprintf(fd,"</FUNCSYNOPSIS>\n");

  // short desc of the function
  //[short_desc sgmlToFile:fs];

  // full desc of the function
  //[desc sgmlToFile:fs];
  
  // parameters description
  
  // return tye description
  // return_desc

  // see also 
  //fprintf(fd, "\nSee also : ");
  //[see_also sgmlToFile:fs];
  
  return self;
}



@end



@implementation felm_root

- addFunction: (id)f
{
  functionList = g_list_append( functionList, (gpointer)f );
  return self;
}



- functionSgmlToFile: (FILE *)fd
{
  int i, nbElem;
  GList *current;

  nbElem = g_list_length(functionList);
  current = functionList;
  for (i=0; i<nbElem; i++)
    {	   
      [(id)current->data sgmlToFile:fd];
      current = g_list_next( current );
    }
  
  return self;
}

@end



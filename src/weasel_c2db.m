/* weasel_c2db.m : main -> extract the sgml doc from a single .c
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "scanner.h"
#include <stdio.h>

gint
main(int argc, char *argv[])
{
  FILE *sgml_fd;
  char *output_name;

//   printf("SCANNING %s\n", argv[1]);
  scanFile( argv[1] );
  [root createFelmList];
  sgml_fd = fopen(argv[2], "w");
  //[root sgmlToFile :sgml_fd];
  [((Weasel_Root *)root)->fileElements functionSgmlToFile:sgml_fd];
  fclose(sgml_fd);
  return 0;
}



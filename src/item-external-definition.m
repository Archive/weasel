/* item-external-definition.m : class defining a 'external-definition' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"

////////////////                 ////////////////
@implementation Weasel_External_Definition

- initWithFuncDefinition: (id) fd
{
  [super init];
  self->func_definition = fd;
  return self;
}

- initWithFuncDefinition: (id) fd
		 comment: (id) c
{
  [super init];
  self->func_definition = fd;
  self->comment=c;
  [self->func_definition addComment:c];
  return self;
}

- initWithDeclaration: (id) decl
{
  [super init];
  self->declaration =decl;
  return self;
}

- (int)isFuncDef
{
  return( self->func_definition != NULL);
}

- sgmlToFile : (FILE *)fd
{
  if (self->func_definition)
    {
      [self->func_definition sgmlToFile :fd];
    }
  if (self->declaration)
    {
      [self->declaration sgmlToFile :fd];
    }
  
  
  return self;
}

- addToFelmList:(id)f
{
  if (self->func_definition)
    {
      [f addFunction:[func_definition createFelm] ];
    }

  return self;
}

@end

 


////////////////                 ////////////////
@implementation Weasel_External_Definition_List
@end




/* felm.h : parent class for all the file elements classes
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef FELM_H
#define FELM_H 1

#include <string.h>
#include <objc/Object.h>
#include "glib.h"

////////////////////////////////
@interface felm_string : Object
////////////////////////////////
{
  char *str;
}

- initWithString: (char *)s;
- (char *)getString;
- sgmlToFile : (FILE *)fd;

@end

////////////////////////////////
@interface felm : Object
////////////////////////////////
{
  id felm_type;
  id name;
  id desc;
  id see_also;
}
- setName: (id)n;
- setDesc: (id)d;
- setSeeAlso : (id)sa;

- sgmlToFile : (FILE *)fd;


@end

////////////////////////////////
@interface felm_function : felm
////////////////////////////////
{
  id short_desc;
  id param_list;
  id return_type;
}

- setParamList: (id)pl;

@end



@interface felm_root : Object
{
  GList *functionList;
}

- addFunction: (id)f;
- functionSgmlToFile: (FILE *)fd;

@end

#endif /* FELM_H */

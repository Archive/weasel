/* item.h : parent  class for all the C grammar elements
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef ITEM_H
#define ITEM_H 1

#include <string.h>
#include <objc/Object.h>
#include "glib.h"





extern id root;

//////////////////////////////////////////////////
@interface Weasel_Item : Object
{
  char *name;
  id comment;

}
- initWithName: (char *)n ;
- setName: (char *) n;
- (char *) getName; 
- sgmlToFile : (FILE *)fd;
- (char *)getSgmlText;
- addComment:(id)c;
@end


//////////////////////////////////////////////////
@interface Weasel_Item_List : Weasel_Item
{
  GList *children; 

}

- addItem: (id) item;
- (int)nbItem; 
- (GList *)getChildren;

@end





#include "item-declaration.h"
#include "item-root.h"
#include "item-declarator.h"
#include "item-storage-class.h"
#include "item-enum.h"
#include "item-struct_union.h"
#include "item-identifier.h"
#include "item-type-name.h"
#include "item-parameter-declaration.h"
#include "item-type-specifier.h"
#include "item-function-definition.h"
#include "item-external-definition.h"
#include "item-file.h"
#include "item-pointer.h"














#endif /* ITEM_H */

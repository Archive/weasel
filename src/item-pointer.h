/* item-pointer.h : class defining a 'pointer' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ITEM_POINTER_H
#define ITEM_POINTER_H 1

#include "item.h"


/////// POINTERS ////////
// %%%%%%%%%%%%%%%%%%%% /
@interface Weasel_Pointer: Weasel_Item
{
  id pointer;
  id type_specifier_list;
}

- initWithTypeSpecifierList: (id)tsl
		    pointer:(id) p ;

@end




#endif /* ITEM_POINTER_H */

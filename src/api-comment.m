/* api-comment.m : classes defining an documentation comment
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "api-comment.h"



/////////////////////////////////////
@implementation Weasel_Api_Line 
/////////////////////////////////////

- initWithString: (char *) l
{
  [super init];
  line = strdup(l);
  return self;
}



- (char *)getLine
{
  return line;
}

@end

@implementation Weasel_Api_Line_List

-initWithLine: (id)l
{
  [super init];
  [self addLine:l];
  return self;
}

- addLine:(id) l
{
  line_list = g_list_append( line_list, (gpointer)l );
  return self;
}

- (char *)getLine
{
  guint nbElem;
  char **lines;
  GList *current;
  int i;
  int totalLength;
  char *text;
  
  nbElem = g_list_length(line_list);
  if (nbElem)
    {
      lines = (char **)g_malloc( nbElem * sizeof(char *) );
      current = self->line_list;
      totalLength=0;
      
      for (i=0; i<nbElem; i++)
	{	   
	  lines[i] = [(id)current->data getLine]; 
	  
	  totalLength += strlen(lines[i]) + 1;
	  current = g_list_next( current );
	}
  
      text = (char *)g_malloc(  (totalLength+1) * sizeof(char));
      sprintf(text, "%s", lines[0]);
      free( lines[0] );
      for (i=1; i<nbElem; i++)
	{	   
	  sprintf(text, "%s %s",text, lines[i]);
	  free( lines[i] );
	}
      free(lines);
      return text;
    }
  else 
    return strdup("");

}

@end




/////////////////////////////////////
@implementation Weasel_Api_Field
/////////////////////////////////////

- initWithKey: (char *) k
       value: (id)v
{
  [super init];
  key = strdup(k);
  value = v;
  return self;
}

- (char *)getKey
{
  return strdup(key);
}

- (id)getValue
{
  return value;
}

- (char)matchKey: (char *)k
{
  if (!k) return 0;
  return (!strcmp(key,k) );
}

@end

/////////////////////////////////////
@implementation Weasel_Api_Param 
/////////////////////////////////////
@end

/////////////////////////////////////
@implementation Weasel_Api_Field_List
/////////////////////////////////////

- initWithField:(id)f
{
  [super init];
  [self addField:f];
  return self;
}

- addField: (id)f
{
  fields = g_list_append( fields, (gpointer)f);
  return self;
}

- (GList *)getList
{
  return fields;
}

- (char *)getValueFromKey: (char *)k
{
  int nbElem,i;
  GList *current;
  id value;

  nbElem = g_list_length(fields);
  current = fields;
  for (i=0; i<nbElem; i++)
    {
      if ( [(id)current->data matchKey:k] ) {
	value = [(id)current->data getValue];
	return( [value getLine] );
      }
      current = g_list_next( current );
    } 
  return( strdup("") );
}

@end




/////////////////////////////////////
@implementation Weasel_Api_Comment
/////////////////////////////////////
- initWithFieldList : (id)fl
{
  [super init];
  fieldList = fl;
  return self;
}

- (char *)getValueFromKey: (char *)k
{
  return [fieldList getValueFromKey:k];
}

@end

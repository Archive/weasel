/* item-parameter-declaration.h : class defining a 'parameter-declaration' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ITEM_PARAMETER_DECLARATION_H
#define ITEM_PARAMETER_DECLARATION_H 1

#include "item.h"


//////////////// PARAMETERS ///////////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% //
@interface Weasel_Parameter_Declaration : Weasel_Item
{
  id type_spec_lst;
  id declarator;
  id type_name;
  char *comment_line;
}
- initWithTypeSpec: (id)ts
	 decl: (id)decl;
- initWithTypeName: (id)tn;
- sgmlDescriptionToFile : (FILE *)fd;

@end

@interface Weasel_Parameter_Declaration_List : Weasel_Item_List
{
  int with_ellipsis;
}
- addElipsisParameter;
- sgmlDescriptionToFile : (FILE *)fd;
@end



#endif /* ITEM_PARAMETER_DECLARATION_H */

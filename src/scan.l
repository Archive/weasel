D			[0-9]
L			[a-zA-Z_]
H			[a-fA-F0-9]
WO			[^@: \n(*/)]
LI			[^@:\n(*/)]
E			[Ee][+-]?{D}+
FS			(f|F|l|L)
IS			(u|U|l|L)*

%x api_comment

%{

/* scan : C scanner
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "gram.h"
#include "item.h"
/*#define YY_DECL int weaser_lexscan( void )*/
extern char *current_filename;

void count();  
%}

%%  

"/**"			{ count(); BEGIN(api_comment);  return(API_COMMENT_START); }

<api_comment>" "*"*/"		{ count(); BEGIN(INITIAL); return(API_COMMENT_END); }
<api_comment>^" "*"*"" "*"@"	{ count(); return(PARAM_DECL); }
<api_comment>^" "*"*"		{ count(); return(STAR); }
<api_comment>":"		{ count(); return(':'); }
<api_comment>"@"		{ count(); return('@'); }
<api_comment>{LI}+$	 	{ yylval = strdup( yytext );count(); return(API_LINE); }
<api_comment>{WO}+		{ yylval = strdup( yytext );count(); return(API_WORD); }
<api_comment>[ \t\v\n\f]	{ count(); }

"#".*\n			{ inclusion(); }
"/*"			{ comment(); }
"//".*\n		{ cpp(); }

"id"			{ yylval = strdup( yytext ); count(); return(ID); }

"auto"			{ yylval = strdup( yytext );count(); return(AUTO); }
"break"			{ count(); return(BREAK); }
"case"			{ count(); return(CASE); }
"char"			{ yylval = strdup( yytext ); count(); return(CHAR); }
"const"			{ yylval = strdup( yytext ); count(); return(CONST); }
"continue"		{ count(); return(CONTINUE); }
"default"		{ count(); return(DEFAULT); }
"do"			{ count(); return(DO); }
"double"		{ yylval = strdup( yytext ); count(); return(DOUBLE); }
"else"			{ count(); return(ELSE); }
"enum"			{ count(); return(ENUM); }
"extern"		{ yylval = strdup( yytext );count(); return(EXTERN); }
"float"			{ yylval = strdup( yytext ); count(); return(FLOAT); }
"for"			{ count(); return(FOR); }
"goto"			{ count(); return(GOTO); }
"if"			{ count(); return(IF); }
"inline"		{ yylval = strdup( yytext );count(); return(INLINE); }
"int"			{ yylval = strdup( yytext ); count(); return(INT); }
"long"			{ yylval = strdup( yytext ); count(); return(LONG); }
"register"		{ yylval = strdup( yytext );count(); return(REGISTER); }
"return"		{ count(); return(RETURN); }
"short"			{ yylval = strdup( yytext ); count(); return(SHORT); }
"signed"		{ yylval = strdup( yytext ); count(); return(SIGNED); }
"sizeof"		{ count(); return(SIZEOF); }
"static"		{ yylval = strdup( yytext );count(); return(STATIC); }
"struct"		{ count(); return(STRUCT); }
"switch"		{ count(); return(SWITCH); }
"typedef"		{ yylval = strdup( yytext );count(); return(TYPEDEF); }
"union"			{ count(); return(UNION); }
"unsigned"		{ yylval = strdup( yytext ); count(); return(UNSIGNED); }
"void"			{ yylval = strdup( yytext ); count(); return(VOID); }
"volatile"		{ yylval = strdup( yytext ); count(); return(VOLATILE); }
"while"			{ count(); return(WHILE); }
"__attribute__"		{ count(); return(ATTRIBUTE); }

{L}({L}|{D})*		{ count(); 
			/* IDENTIFIERS : variable names, function names ... */
			/* give the text to bison */
			yylval = strdup( yytext );
			return(check_type());
			}

0[xX]{H}+{IS}?		{ count(); return(CONSTANT); }
0{D}+{IS}?		{ count(); return(CONSTANT); }
{D}+{IS}?		{ count(); return(CONSTANT); }
'(\\.|[^\\'])+'		{ count(); return(CONSTANT); }

{D}+{E}{FS}?		{ count(); return(CONSTANT); }
{D}*"."{D}+({E})?{FS}?	{ count(); return(CONSTANT); }
{D}+"."{D}*({E})?{FS}?	{ count(); return(CONSTANT); }

\"(\\.|[^\\"])*\"	{ yylval = strdup( yytext );count(); return(STRING_LITERAL); }

">>="			{ count(); return(RIGHT_ASSIGN); }
"<<="			{ count(); return(LEFT_ASSIGN); }
"+="			{ count(); return(ADD_ASSIGN); }
"-="			{ count(); return(SUB_ASSIGN); }
"*="			{ count(); return(MUL_ASSIGN); }
"/="			{ count(); return(DIV_ASSIGN); }
"%="			{ count(); return(MOD_ASSIGN); }
"&="			{ count(); return(AND_ASSIGN); }
"^="			{ count(); return(XOR_ASSIGN); }
"|="			{ count(); return(OR_ASSIGN); }
">>"			{ count(); return(RIGHT_OP); }
"<<"			{ count(); return(LEFT_OP); }
"++"			{ count(); return(INC_OP); }
"--"			{ count(); return(DEC_OP); }
"->"			{ count(); return(PTR_OP); }
"&&"			{ count(); return(AND_OP); }
"||"			{ count(); return(OR_OP); }
"<="			{ count(); return(LE_OP); }
">="			{ count(); return(GE_OP); }
"=="			{ count(); return(EQ_OP); }
"!="			{ count(); return(NE_OP); }
";"			{ count(); return(';'); }
"{"			{ count(); return('{'); }
"}"			{ count(); return('}'); }
","			{ count(); return(','); }
":"			{ count(); return(':'); }
"="			{ count(); return('='); }
"("			{ count(); return('('); }
")"			{ count(); return(')'); }
"["			{ count(); return('['); }
"]"			{ count(); return(']'); }
"."			{ count(); return('.'); }
"&"			{ count(); return('&'); }
"!"			{ count(); return('!'); }
"~"			{ count(); return('~'); }
"-"			{ count(); return('-'); }
"+"			{ count(); return('+'); }
"*"			{ count(); return('*'); }
"/"			{ count(); return('/'); }
"%"			{ count(); return('%'); }
"<"			{ count(); return('<'); }
">"			{ count(); return('>'); }
"^"			{ count(); return('^'); }
"|"			{ count(); return('|'); }
"?"			{ count(); return('?'); }
"..."			{ count(); return(ELIPSIS); }
"#"			{ count(); return('#'); }
[ \t\v\n\f]		{ count(); }
.			{ /* ignore bad characters */ }

%%


comment()
{
	char c, c1;

loop:
	while ((c = input()) != '*' && c != 0)
		putchar(c);

	if ((c1 = input()) != '/' && c != 0)
	{
		unput(c1);
		goto loop;
	}

	if (c != 0)
		putchar(c1);
}

cpp()
{

}

int column = 0;

void count()
{
	int i;

	for (i = 0; yytext[i] != '\0'; i++)
		if (yytext[i] == '\n')
			column = 0;
		else if (yytext[i] == '\t')
			column += 8 - (column % 8);
		else
			column++;

	ECHO;
}

int check_type()
{

  yylval = strdup( yytext );	
  
  if ([root  knowsType:yytext])
    {
      return (TYPE_NAME);
    }
  else
    {
      return(IDENTIFIER);
    }
}


inclusion()
{
	char *tmpstr;
	if (current_filename) free(current_filename);
	tmpstr = strdup(yytext);
	sscanf(yytext,"# %*d %s", tmpstr);
	//printf("########### NEW FILE : %s\n", tmpstr);
	current_filename = tmpstr;
}

setInputFile( char *input)
{
  FILE *fp;

  fp = (FILE *)fopen( input, "r");
  yyin = fp;
  //yyout= (FILE *)NULL;
  yyout= (FILE *)fopen( "/dev/null", "rw");


}

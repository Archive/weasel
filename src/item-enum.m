/* item-enum.m : class defining a 'enum' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"

/////////// ENUM  SPECIFIER///////////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% //

@implementation Weasel_Enum_Specifier

-initWith: (id) idt
enumerator_list: (id)list
{
  [super init];
  self->identifier = idt;
  self->enumerator_list = list;
  return self;
}


@end



@implementation Weasel_Enumerator

- initWithIdentifier: (id)idt
	 initializer: (id)init
{
  [super init];
  self->identifier = idt;
  self->initializer = init;
  return self;
}


@end


@implementation Weasel_Enumerator_List
@end


/* item-declaration.h : class defining a 'declaration' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ITEM_DECLARATION_H
#define ITEM_DECLARATION_H 1

#include "item.h"



////////////////////////////////////////////
//////////   Declarations //////////////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% //
@interface Weasel_Declaration : Weasel_Item
{
  id declSpecif;
  id init_list;
}

- initWithDeclaration: (id)decl
	   initList: (id)initlst;
@end

@interface Weasel_Declaration_Specifier : Weasel_Item
{
  id storage_class;
  id type;
  id declaration;
}

- initWithStorageClass: (id)sc
	   declaration: (id)decl;
- initWithType: (id)type
   declaration: (id)decl;
- (id)getStorageClass;
@end

@interface Weasel_Init_Declarator : Weasel_Item
{
  id declarator;
  id initializer;
 
}
- initWithDeclarator: (id)decl
	 initializer: (id)init;
@end

@interface Weasel_Init_Declarator_List: Weasel_Item_List
@end

#endif /* ITEM_DECLARATION_H */

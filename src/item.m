/* item.m : parent  class for all the C grammar elements
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"


//////////////               //////////////////
@implementation Weasel_Item 

- initWithName: (char *)n 
{
  
  [self setName: n];
  return [self init];
}


- setName: (char *) n
{
  
  self->name = n;
  return self;
}



- (char *) getName
{
  if (name)
    return strdup( name );
  else
    return strdup("item" );
}


- sgmlToFile : (FILE *)fd;
{
  return self;
}

- (char *)getSgmlText
{
  return( strdup("") );
}


- addComment:(id)c
{
  self->comment = c;
  return self;
}
@end




////////////////                 ////////////////
@implementation Weasel_Item_List 

- init
{
  [super init];
  //self->children = g_list_alloc();
  return self;
}

- addItem: (id) item
{
  self->children = g_list_append( self->children, (gpointer)item );
    
  return self;
}

- (int)nbItem
{
  if (!children) return 0;
  return( g_list_length(children) ); 
}



- (GList *)getChildren
{
  return self->children;
}

- sgmlToFile : (FILE *)fd
{
  guint nbElem;
  GList *current;
  int i;
 
  nbElem = g_list_length(children);
  current = children;
  for (i=0; i<nbElem; i++)
    {	   
      [(id)current->data sgmlToFile:fd];
      current = g_list_next( current );
    }
  return self;
}

- addComment :(id)c
{
  guint nbElem;
  GList *current;
  int i;
 
  nbElem = g_list_length(children);
  current = children;
  for (i=0; i<nbElem; i++)
    {	   
      [(id)current->data addComment:c];
      current = g_list_next( current );
    }
  return self;
}
@end
















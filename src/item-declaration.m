/* item-declaration.m : class defining a 'declaration' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "item.h"


////////////////////////////////////////////
//////////   Declarations //////////////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% //

@implementation Weasel_Declaration

- initWithDeclaration: (id)decl
	   initList: (id)initlst
{
  [super init];
  self->declSpecif = decl;
  self->init_list = initlst;
  [self checkIfNewTypedef];
  return self;

}

- checkIfNewTypedef
{
  GList *initList;
  int nbElem, i;
  GList *current;
  id sc;
  char *typeName, *testedTypeName;
  
  sc = [declSpecif getStorageClass];
  testedTypeName=[sc getName];
  if (testedTypeName && !strcmp( testedTypeName, "typedef" ) )
    {
      initList = [init_list getChildren];
      nbElem = g_list_length(initList); 
      current = initList;
      for (i=0; i<nbElem; i++)
	{	   
	  typeName=[[(id)(current->data) getDeclarator] getName];
	  [ root addNewTypedef:typeName];
	  current = g_list_next( current );
	}
    }
  if (testedTypeName) free(testedTypeName);
  return self;
}

- sgmlToFile : (FILE *)fd
{
  if (self->declSpecif)
    [  self->declSpecif sgmlToFile:fd];
  if (self->init_list)
    [  self->init_list sgmlToFile:fd];
  return self;
}

@end


@implementation Weasel_Declaration_Specifier

- initWithStorageClass: (id)sc
	   declaration: (id)decl
{
  [super init];
  self->declaration = decl;
  self->storage_class = sc;
  return self;

}

- initWithType: (id)t
   declaration: (id)decl
{
  [super init];
  self->declaration = decl;
  self->type = t;
  return self;

}
- (id)getStorageClass
{
  return storage_class;
}




- sgmlToFile : (FILE *)fd
{
  if (self->type)
    [  self->type sgmlToFile:fd];
  if (self->storage_class)
    [  self->storage_class sgmlToFile:fd];
  if (self->declaration)
    [  self->declaration sgmlToFile:fd];
  return self;
}


@end


@implementation Weasel_Init_Declarator

- initWithDeclarator: (id)decl
	 initializer: (id)init
{
  [super init];
  self->declarator = decl;
  self->initializer = init;
  return self;
}

- (id)getDeclarator
{
  return declarator;
}


- sgmlToFile : (FILE *)fd
{
  if (self->declarator)
    [  self->declarator sgmlToFile:fd];
  return self;
}

@end

@implementation Weasel_Init_Declarator_List

- sgmlToFile : (FILE *)fd
{
  guint nbElem;
  GList *current;
  int i;
 
  nbElem = g_list_length(children);
  current = children;
  if (nbElem)
    {
      [(id)current->data sgmlToFile:fd];
      current = g_list_next( current );
    }
  for (i=1; i<nbElem; i++)
    {
      fprintf(fd,", "); 
      [(id)current->data sgmlToFile:fd];
      current = g_list_next( current );
    }

}

@end


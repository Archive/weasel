/* item-root.m : class containing all the declarations found
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"
#include "felm.h"

///////////////////////////////////////////
/////////// Root //////////////////////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% //
@implementation Weasel_Root 


- init
{
  [super init];
  fileElements = [felm_root alloc];
  
  return self;
}


- addNewTypedef: (char *)nt
{
  self->type_list=g_list_append( self->type_list, (gpointer)strdup(nt) );
  
  return self;  
}

- (int)knowsType: (char *)t
{
  GList *current;
  int test;
  
  test=0;
  current = self->type_list;
  while ( (current != NULL) && (!test) ) 
    {
      test = !strcmp( current->data, t );
      current=g_list_next(current);
    }

  return test;  
}

/**
 * retreives or creates a file in the root file list
 */
- getFile : (char *)filename
{

  GList *current;
  id new_file;
  char *current_file_name;
  int test;
  
   test=0;
   
   /* try to find the file in the file list */
   current = self->file_list;
   while ( (current != NULL) ) 
     {
       current_file_name = [(id)current->data getName];
       test = !strcmp(current_file_name, filename );
       free(current_file_name);
       if (test) return((id)current->data);
       current=g_list_next(current);
     }
   
   /* if we are here, the file does not already exist */
   new_file = [[  Weasel_File alloc] initWithName:filename]; 
   self->file_list=g_list_append( self->file_list, (gpointer)new_file );
   return new_file;
}

/**
 * stores some external declaration in a file
 * the file is referenced with its complete path between ""
 * (because it comes directly from gram.y) 
 */
- storeFile : (char *)filename
declaration: (id)dl
{
  char *tmpstr;
  int len;
  id file;

  len=strlen((char *)filename);
  tmpstr=strdup(((char *)filename) +1); /* skip the first " */
  tmpstr[len-2]='\0'; /* skip the last " */ 
  file = [self getFile : tmpstr];
  [file addExternDeclaration : dl];
  //[dl free];
  return self;

}


- sgmlToFile : (FILE *)fd
{
  id first_file;
  
  first_file = (self->file_list)->data;
  [first_file sgmlToFile :fd];
  return self;

}

- createFelmList
{
  id first_file;

  first_file = (self->file_list)->data;
  [first_file addToFelmList:fileElements];
  return self;

}

@end


@implementation Weasel_Flag_List : Weasel_Item_List

- (int)isSomeNewText
{
  GList *current;
  int test;
  
  test=0;
  current = self->children;
  while ( (current != NULL) && (!test) ) 
    {
      test = (int)(current->data)==1;
      current=g_list_next(current);
    }

  return test;
} 

@end

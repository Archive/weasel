/* item-storage-class.h : class for the 'storage-class' grammar element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef ITEM_STORAGE_CLASS_H
#define ITEM_STORAGE_CLASS_H 1

#include "item.h"


/////// STORAGE CLASS SPECIFIERS ////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% /
@interface Weasel_Storage_Class_Specifier: Weasel_Item
@end




#endif /* ITEM_STORAGE_CLASS_H */

%{
/* gram : C grammar parser
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <stdio.h>
#define YYSTYPE id
#include "item.h"
#include "api-comment.h"
#include "scanner.h"
char *current_filename;
//#define YYDEBUG 1
//extern int yydebug=1;
%}

%token IDENTIFIER CONSTANT STRING_LITERAL SIZEOF
%token PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN TYPE_NAME

%token TYPEDEF EXTERN STATIC AUTO REGISTER INLINE
%token CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID ID
%token STRUCT UNION ENUM ELIPSIS RANGE ATTRIBUTE LINE

%token CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%token API_COMMENT_START STAR API_COMMENT_END API_LINE API_WORD PARAM_DECL


%start file
%%



/* API COMMENT DESCRIPTION */

an_api_line
	: API_LINE
	{  
		$$=[[Weasel_Api_Line alloc] initWithString:(char *)$1 ];
	}
	| STAR API_LINE
	{
		$$=[[Weasel_Api_Line alloc] initWithString:(char *)$2 ];
	}
	;
api_line_list
	: an_api_line
	{  
		$$=[[Weasel_Api_Line_List alloc] initWithLine:$1 ];
	}
	| api_line_list an_api_line
	{
		$$=[$1 addLine:$2];
	}
	;
api_param
	: PARAM_DECL API_WORD ':' api_line_list
	{
		$$=[[Weasel_Api_Param alloc] initWithKey:(char *)$2 value:$4];
	}
	;

api_field
	: api_param
	;  

api_field_list
	: api_field
	{
		$$=[[Weasel_Api_Field_List alloc] initWithField:$1];
	}
	| api_field_list api_field
	{
		$$=[$1 addField:$2];
	}
	;

api_comment
	: API_COMMENT_START API_COMMENT_END	
	{
		$$=[[Weasel_Api_Comment alloc] initWithFieldList:NULL];
	}
	| API_COMMENT_START api_field_list API_COMMENT_END	
	{
		$$=[[Weasel_Api_Comment alloc] initWithFieldList:$2];
	}	
	;


/* C CODE DESCRIPTION */
primary_expr
	: identifier
	| CONSTANT
	| STRING_LITERAL
	| '(' expr ')'
	;

postfix_expr
	: primary_expr
	| postfix_expr '[' expr ']'
	| postfix_expr '(' ')'
	| postfix_expr '(' argument_expr_list ')'
	| postfix_expr '.' identifier
	| postfix_expr PTR_OP identifier
	| postfix_expr INC_OP
	| postfix_expr DEC_OP
	;

argument_expr_list
	: assignment_expr
	| argument_expr_list ',' assignment_expr
	;

unary_expr
	: postfix_expr
	| INC_OP unary_expr
	| DEC_OP unary_expr
	| unary_operator cast_expr
	| SIZEOF unary_expr
	| SIZEOF '(' type_name ')'
	;

unary_operator
	: '&'
	| '*'
	| '+'
	| '-'
	| '~'
	| '!'
	;

cast_expr
	: unary_expr
	| '(' type_name ')' cast_expr
	;

multiplicative_expr
	: cast_expr
	| multiplicative_expr '*' cast_expr
	| multiplicative_expr '/' cast_expr
	| multiplicative_expr '%' cast_expr
	;

additive_expr
	: multiplicative_expr
	| additive_expr '+' multiplicative_expr
	| additive_expr '-' multiplicative_expr
	;

shift_expr
	: additive_expr
	| shift_expr LEFT_OP additive_expr
	| shift_expr RIGHT_OP additive_expr
	;

relational_expr
	: shift_expr
	| relational_expr '<' shift_expr
	| relational_expr '>' shift_expr
	| relational_expr LE_OP shift_expr
	| relational_expr GE_OP shift_expr
	;

equality_expr
	: relational_expr
	| equality_expr EQ_OP relational_expr
	| equality_expr NE_OP relational_expr
	;

and_expr
	: equality_expr
	| and_expr '&' equality_expr
	;

exclusive_or_expr
	: and_expr
	| exclusive_or_expr '^' and_expr
	;

inclusive_or_expr
	: exclusive_or_expr
	| inclusive_or_expr '|' exclusive_or_expr
	;

logical_and_expr
	: inclusive_or_expr
	| logical_and_expr AND_OP inclusive_or_expr
	;

logical_or_expr
	: logical_and_expr
	| logical_or_expr OR_OP logical_and_expr
	;

conditional_expr
	: logical_or_expr
	| logical_or_expr '?' logical_or_expr ':' conditional_expr
	;

assignment_expr
	: conditional_expr
	| unary_expr assignment_operator assignment_expr
	;

assignment_operator
	: '='
	| MUL_ASSIGN
	| DIV_ASSIGN
	| MOD_ASSIGN
	| ADD_ASSIGN
	| SUB_ASSIGN
	| LEFT_ASSIGN
	| RIGHT_ASSIGN
	| AND_ASSIGN
	| XOR_ASSIGN
	| OR_ASSIGN
	;

/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
/* Expressions */
expr
	: assignment_expr
	| expr ',' assignment_expr
	;

constant_expr
	: conditional_expr
	;

declaration
	: declaration_specifiers ';'
	{
		id decl;
		decl= [[Weasel_Declaration alloc] initWithDeclaration:$1
				initList:NULL];
		$$=decl;
		
	}
	| declaration_specifiers init_declarator_list ';'
	{
		id decl;
		decl= [[Weasel_Declaration alloc] initWithDeclaration:$1
				initList:$2];
		$$=decl;
		
	}
	;

declaration_specifiers
	: storage_class_specifier
	{
		$$=[[Weasel_Declaration_Specifier alloc] initWithStorageClass: $1 declaration: NULL];
	}
	| storage_class_specifier declaration_specifiers
	{
		$$=[[Weasel_Declaration_Specifier alloc] initWithStorageClass: $1 declaration: $2];
	}
	| type_specifier
	{
		$$=[[Weasel_Declaration_Specifier alloc] initWithType: $1 declaration: NULL];
	}
	| type_specifier declaration_specifiers
	{
		$$=[[Weasel_Declaration_Specifier alloc] initWithType: $1 declaration: $2];
	}
	;

init_declarator_list
	: init_declarator
		{ $$=[[ Weasel_Init_Declarator_List new] addItem:$1]; }
	| init_declarator_list ',' init_declarator
		{ $$=[ $1 addItem:$2]; }
	;

init_declarator
	: declarator
	{
		$$=[[Weasel_Init_Declarator alloc] initWithDeclarator:$1 initializer:NULL];
	}
	| declarator '=' initializer
	{
		$$=[[Weasel_Init_Declarator alloc] initWithDeclarator:$1 initializer:$3];
	}
	;

storage_class_specifier
	: TYPEDEF { $$=[[Weasel_Storage_Class_Specifier alloc] initWithName:(char *)$1]; }
	| EXTERN { $$=[[Weasel_Storage_Class_Specifier alloc] initWithName:(char *)$1]; }
	| STATIC { $$=[[Weasel_Storage_Class_Specifier alloc] initWithName:(char *)$1]; }
	| AUTO { $$=[[Weasel_Storage_Class_Specifier alloc] initWithName:(char *)$1]; }
	| REGISTER { $$=[[Weasel_Storage_Class_Specifier alloc] initWithName:(char *)$1]; }
	| INLINE { $$=[[Weasel_Storage_Class_Specifier alloc] initWithName:(char *)$1]; }
	;

type_specifier
	: CHAR { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| SHORT { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| INT { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| LONG { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| SIGNED { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| UNSIGNED { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| FLOAT { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| DOUBLE { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| CONST { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| VOLATILE { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| VOID { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| ID { $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	| struct_or_union_specifier
	| enum_specifier
	| TYPE_NAME { 	  $$=[[Weasel_Type_Specifier alloc] initWithName:(char *)$1]; }
	;

struct_or_union_specifier
	: struct_or_union identifier '{' struct_declaration_list '}'
		{
			$$= [[Weasel_Struc_Union_Specifier alloc] initWith:(char *)$1
					identifier:$2
					declaration_list:$4];
			
		}
	| struct_or_union '{' struct_declaration_list '}'
		{
			$$= [[Weasel_Struc_Union_Specifier alloc] initWith:(char *)$1
					identifier:NULL
					declaration_list:$3];
		}
	| struct_or_union identifier
		{
			$$= [[Weasel_Struc_Union_Specifier alloc] initWith:(char *)$1
					identifier:$2
					declaration_list:NULL];
		}
	;

struct_or_union
	: STRUCT
		{ $$=(id)"struct"; }
	| UNION
		{ $$=(id)"union"; }
	;

struct_declaration_list
	: struct_declaration 
		{ $$=[[ Weasel_Struct_Declaration_List new] addItem:$1]; }
	
	| struct_declaration_list struct_declaration
		{ $$=[ $1 addItem:$2]; }
	;

struct_declaration
	: type_specifier_list struct_declarator_list ';'
	{
		 $$= [[Weasel_Struct_Declaration alloc] 
			initWithTypeList:$1  declaratorList:$2];
	}
	;

struct_declarator_list
	: struct_declarator
		{ $$=[[ Weasel_Struct_Declarator_List new] addItem:$1]; }
	| struct_declarator_list ',' struct_declarator
		{ $$=[ $1 addItem:$3]; }
	;

struct_declarator
	: declarator
	{
		$$=[[Weasel_Struct_Declarator alloc] 
			initWithDeclarator: $1 initializer:NULL];
	}
	| ':' constant_expr
	{
		$$=[[Weasel_Struct_Declarator alloc] 
			initWithDeclarator: NULL initializer:$2];
	}
	| declarator ':' constant_expr
	{
		$$=[[Weasel_Struct_Declarator alloc] 
			initWithDeclarator: $1 initializer:$3];
	}
	;

enum_specifier
	: ENUM '{' enumerator_list '}'
	{
		$$=[[Weasel_Enum_Specifier new] 
			initWith: NULL enumerator_list:$3];
	}
	| ENUM identifier '{' enumerator_list '}'
	{
		$$=[[Weasel_Enum_Specifier new] 
			initWith:$2  enumerator_list:$4];
	}
	| ENUM identifier
	{
		$$=[[Weasel_Enum_Specifier new] 
			initWith:$2  enumerator_list:NULL];
	}
	;

enumerator_list
	: enumerator
		{ $$=[[ Weasel_Enumerator_List new] addItem:$1]; }
	| enumerator_list ',' enumerator
		{ $$=[ $1 addItem:$3]; }
	;

enumerator
	: identifier
	{	
		$$=[[Weasel_Enumerator alloc]
			initWithIdentifier:$1 initializer:NULL];
	}
	| identifier '=' constant_expr
	{	
		$$=[[Weasel_Enumerator alloc]
			initWithIdentifier:$1 initializer:$3];
	}
	;

declarator
	: declarator2
	| pointer declarator2
	{
		$$=[$2 addPointer :$1];
	}	
	;

declarator2
	: identifier
	{
		$$=[[Weasel_Declarator alloc] initWithDeclarator: $1];
	}	
	| '(' declarator ')'
	{
		$$=[$2 addBracket]; 
	}
	| declarator2 '[' ']'
	{
		$$=[[Weasel_Array_Declarator alloc] initWithDeclarator: $1 index: NULL ];
	}
	| declarator2 '[' constant_expr ']'
	{
		$$=[[Weasel_Array_Declarator alloc] initWithDeclarator: $1 index: $3 ];
	}
	| declarator2 '(' ')'
	{
		$$=[[Weasel_Function_Declarator alloc] initWithDeclarator: $1 paramList: NULL ];
	}
	| declarator2 '(' parameter_type_list ')' 
	{
		$$=[[Weasel_Function_Declarator alloc] initWithDeclarator: $1 paramList: $3 ];
	}	
	| declarator2 '(' parameter_identifier_list ')'
	{
		$$=[[Weasel_Function_Declarator alloc] initWithDeclarator: $1 paramList: $3 ];
	}
	| declarator2 ATTRIBUTE '(' '(' identifier  '(' identifier ','  CONSTANT  ','   CONSTANT ')' ')' ')'
	;

pointer
	: '*'
	{
		$$=[[Weasel_Pointer alloc] initWithTypeSpecifierList: NULL pointer:NULL ];
	}
	| '*' type_specifier_list
	{
		$$=[[Weasel_Pointer alloc] initWithTypeSpecifierList: $2 pointer:NULL ];
	}
	| '*' pointer
	{
		$$=[[Weasel_Pointer alloc] initWithTypeSpecifierList: NULL pointer:$2 ];
	}
	| '*' type_specifier_list pointer
	{
		$$=[[Weasel_Pointer alloc] initWithTypeSpecifierList: $1 pointer:$2 ];
	}
	;

type_specifier_list
	: type_specifier
	{	
		$$=[[ Weasel_Type_Specifier_List new] addItem:$1];
	}	
	| type_specifier_list type_specifier
	{
		$$=[ $1 addItem:$2];
	}
	;

parameter_identifier_list
	: identifier_list
	| identifier_list ',' ELIPSIS
	{
		$$=[ $1 addItem:
			[ [Weasel_Identifier alloc] initWithName:"..." ]
			];
	}
	;

identifier_list
	: identifier
	{	
		$$=[[ Weasel_Identifier_List new] addItem:$1];
	}
	| identifier_list ',' identifier
	{
		$$=[ $1 addItem:$3];
	}
	;

parameter_type_list
	: parameter_list
	| parameter_list ',' ELIPSIS
	{
		$$=[$1 addElipsisParameter];
	}
	;

parameter_list
	: parameter_declaration
	{	
		$$=[[ Weasel_Parameter_Declaration_List new] addItem:$1];
	}
	| parameter_list ',' parameter_declaration
	{
		$$=[ $1 addItem:$3];
	}
	;

parameter_declaration
	: type_specifier_list declarator
	{	
		$$=[[ Weasel_Parameter_Declaration new] initWithTypeSpec:$1 decl:$2];
	}
	| type_name
	{	
		$$=[[ Weasel_Parameter_Declaration new] initWithTypeName:$1];
	}
	;

type_name
	: type_specifier_list
	{	
		$$=[[ Weasel_Type_Name new] initWithTypeSpec:$1 absDecl:NULL];
	}
	| type_specifier_list abstract_declarator
	{	
		$$=[[ Weasel_Type_Name new] initWithTypeSpec:$1 absDecl:$2];
	}
	;

/* to finish */
abstract_declarator
	: pointer
	| abstract_declarator2
	| pointer abstract_declarator2
	{	printf("vroummmmmmm\n");
		$$=$2;
	}	
	;

/* to finish */
 /* Have to define _abstract_  objects in item.m */ 
abstract_declarator2
	: '(' abstract_declarator ')'
	{
		$$=$2
	}
	| '[' ']'
	| '[' constant_expr ']'
	| abstract_declarator2 '[' ']'
	{
		$$=[[Weasel_Array_Declarator alloc] initWithDeclarator: $1 index: NULL ];
	}
	| abstract_declarator2 '[' constant_expr ']'
	{
		$$=[[Weasel_Array_Declarator alloc] initWithDeclarator: $1 index: $3 ];
	}
	| '(' ')'
	| '(' parameter_type_list ')'
	| abstract_declarator2 '(' ')'
	{
		$$=[[Weasel_Function_Declarator alloc] initWithDeclarator: $1 paramList: NULL ];
	}
	| abstract_declarator2 '(' parameter_type_list ')'
	{
		$$=[[Weasel_Function_Declarator alloc] initWithDeclarator: $1 paramList: $3 ];
	}
	;

/* to implement */
initializer
	: assignment_expr
	| '{' initializer_list '}'
	| '{' initializer_list ',' '}'
	;

/* to implement */
initializer_list
	: initializer
	| initializer_list ',' initializer
	;

statement
	: labeled_statement
	| compound_statement
	| expression_statement
	| selection_statement
	| iteration_statement
	| jump_statement
	;


labeled_statement
	: identifier ':' statement
	| CASE constant_expr ':' statement
	| DEFAULT ':' statement
	;

compound_statement
	: '{' '}'
	| '{' statement_list '}'
	| '{' declaration_list '}'
	| '{' declaration_list statement_list '}'
	;

declaration_list
	: declaration
	| declaration_list declaration
	;

statement_list
	: statement
	| statement_list statement
	;

expression_statement
	: ';'
	| expr ';'
	;


selection_statement
	: IF '(' expr ')' statement
	| IF '(' expr ')' statement ELSE statement
	| SWITCH '(' expr ')' statement
	;


iteration_statement
	: WHILE '(' expr ')' statement
	| DO statement WHILE '(' expr ')' ';'
	| FOR '(' ';' ';' ')' statement
	| FOR '(' ';' ';' expr ')' statement
	| FOR '(' ';' expr ';' ')' statement
	| FOR '(' ';' expr ';' expr ')' statement
	| FOR '(' expr ';' ';' ')' statement
	| FOR '(' expr ';' ';' expr ')' statement
	| FOR '(' expr ';' expr ';' ')' statement
	| FOR '(' expr ';' expr ';' expr ')' statement
	;

jump_statement
	: GOTO identifier ';'
	| CONTINUE ';'
	| BREAK ';'
	| RETURN ';'
	| RETURN expr ';'
	;

flag_list
	: CONSTANT
	{	
		$$=[[ Weasel_Flag_List new] addItem:$1];
	}
	| flag_list CONSTANT
	{
		$$=[ $1 addItem:$2];
	}
	;


file
	: external_definition 
	{	
		[root storeFile : current_filename
			declaration: $1];
	
	}
	| file external_definition
	{
		[root storeFile : current_filename
			declaration: $2];
	}
	;

external_definition
	: api_comment function_definition
		{ 
		$$=[ [Weasel_External_Definition alloc] initWithFuncDefinition: $2 comment:$1];
		}
	| function_definition
		{ 
		$$=[ [Weasel_External_Definition alloc] initWithFuncDefinition: $1 ];
		}
	| declaration
		{ 
		$$=[ [Weasel_External_Definition alloc] initWithDeclaration: $1 ];
		}
	;

function_definition
	: declarator function_body
		{ 
		$$=[ [Weasel_Function_Definition alloc] initWithDeclSpecifer:NULL  
					declarator:$1 ];
		}
	| declaration_specifiers declarator function_body
		{	
		$$=[ [Weasel_Function_Definition alloc] initWithDeclSpecifer:$1  
					declarator:$2 ];
		}
	;

function_body
	: compound_statement
	| declaration_list compound_statement
	;

/* identifers are the function names, the variable names, the type names ... */
identifier
	: IDENTIFIER
		{ 
		$$=[ [Weasel_Identifier alloc] initWithName:(char *) $1 ];
		}
	;
%%

#include <stdio.h>

extern char yytext[];
extern int column;

yyerror(s)
char *s;
{
	//fflush(stdout);
	printf("\n%*s\n%*s\n", column, "^", column, s);
}

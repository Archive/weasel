/* item-declarator.m : class defining a 'declarator' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"


// declarators

////////////////                  ////////////////
@implementation Weasel_Declarator

- initWithDeclarator: (id ) d
{
  [super init];
  [self  setDeclarator: d]; 
  return self;
}

- setDeclarator: (id ) d
{
  declarator=d;
  return self;
}

- (char *) getName
{
  return [declarator getName];;
}

- sgmlToFile: (FILE *)fd
{
  if (self->pointer)
    [self->pointer sgmlToFile:fd];
  return [declarator sgmlToFile:fd];
}


- addBracket
{
  self->withBracket;
  return self;
}

- addPointer : (id)p
{
  self->pointer = p;
  return self;
}
- addComment:(id)c
{
  [declarator addComment:c];
  return self;
}

@end




//////////////////// FUNCTION DECALARATOR /////////////////
////////////////                           ////////////////
@implementation Weasel_Function_Declarator

-initWithDeclarator: (id) d
	  paramList: (id ) plist
{
  [super initWithDeclarator: d];
  [self setParamList: plist];
  return self;
}

- setParamList: (id ) plist
{
  param_list = plist;
  return self;
}


- sgmlFunctionNameToFile : (FILE *)fd
{
  char *identifier_name;

  /* get the identifier of the function def */
  identifier_name = [self->declarator getName];
  
  fprintf(fd,"<function>%s</function>", identifier_name);
  free(identifier_name);

  return self;
}

- (char *)getName
{
  return( [self->declarator getName] );
}


- sgmlParamListToFile : (FILE *)fd
{
  [param_list sgmlToFile:fd];
  return self;
  
}

- sgmlParamDescriptionToFile : (FILE *)fd
{
  [param_list sgmlDescriptionToFile:fd];
  return self;
  
}




- addComment:(id)c
{
  [declarator addComment:c];
  [param_list addComment:c];
  return self;
}

- createParamListFelm
{

}
@end




////////////////                         ////////////////
@implementation Weasel_Array_Declarator
- initWithDeclarator: (id) d
	  index: (id ) ind
{
  [super initWithDeclarator: d];
  [self setIndex: ind];
  return self;
  
}

- sgmlToFile: (FILE *)fd
{
  if (self->declarator)
    [self->declarator sgmlToFile:fd];
  fprintf(fd,"[");
  if (self->index)
    //[self->index sgmlToFile:fd];
  fprintf(fd,"]");

  return [declarator sgmlToFile:fd];
}

- setIndex: (id )ind
{
  index=ind;
  return self;
}

- addComment:(id)c
{
  [declarator addComment:c];
  return self;
}

@end


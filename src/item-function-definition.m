/* item-function-definition.m : class defining a 'function-definition' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "item.h"
#include "felm.h"

////////////////                 ////////////////
@implementation Weasel_Function_Definition

- initWithDeclSpecifer: (id) ds
	     declarator:d
{
  [super init];
  self->declaration_specifiers = ds;
  self->declarator = d;
  return self;
  
}
- addComment:(id)c
{
  [declarator addComment:c];
  return self;
}

- sgmlToFile : (FILE *)fd
{
  char *decl_text;

  
  fprintf(fd,"<funcsynopsis>\n<funcdef>\n"); 
  [self->declarator  sgmlFunctionNameToFile :fd];
  fprintf(fd,"</funcdef>\n");
  [self->declarator  sgmlParamListToFile :fd];
  fprintf(fd,"</funcsynopsis>\n");
  [self->declarator  sgmlParamDescriptionToFile :fd];
  
  return self;
}

- createFelm
{
  id aFelm;
  id declPLFelm;
  char *funcName;
  
  aFelm = [felm_function alloc];
  funcName = [self->declarator getName];
  declPLFelm = [declarator createParamListFelm];

  [aFelm setName:[[felm_string alloc] initWithString:funcName] ];
  [aFelm setParamList:declPLFelm];

  free(funcName);
  
  return aFelm;
  
}
@end




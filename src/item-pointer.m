/* item-pointer.m : class defining a 'pointer' Grammar Element
 *
 * AUTHORS :
 *      Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * Copyright (C) 1998 Free Software Foundation
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "item.h"
/////// TYPE SPECIFIERS ////////
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% /
@implementation Weasel_Pointer

- initWithTypeSpecifierList: (id)tsl
		    pointer:(id) p 
{
  [super init];
  self->pointer = p;
  self->type_specifier_list = tsl;
  return self;
}



- sgmlToFile : (FILE *)fd
{
  fprintf(fd, "*");
  if (type_specifier_list)
    [type_specifier_list sgmlToFile:fd];
  if (pointer)
    [pointer sgmlToFile:fd];
  
  return self;
}
@end

